package dev.tedesco.colecciones;

import java.util.ArrayList;

public class ArrayListExample {
	ArrayList<String> androids = new ArrayList<String>();
	
	public void añadirElementos() {
		androids.add("Cupcake");
		androids.add("Donut");
		androids.add("Elcair");
		androids.add("Froyo");
		androids.add("Gingerbread");
		androids.add("Honeycomb");
	}
	public void imprimir() {
		System.out.println("Tamaño del ArrayList " + androids.size());
		System.out.println("ArrayList " + androids);
		System.out.println();
	}
	
	public void eliminar() {
		try{
			androids.remove(4);
			System.out.println("Se a aplicado un remove del elemento con indice 4");
			System.out.println();
			System.out.println("Nuevo tamaño del ArrayList " + androids.size());
			System.out.println("ArrayList " + androids);
		}catch(ArrayIndexOutOfBoundsException arrayE) {
			System.out.println("Estas intentando eliminar un indice que no existe");
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println();
		}
		
	}
}
