package dev.tedesco.colecciones;

import java.util.Vector;

public class VectorExample {
	Vector<String> androidsV2 = new Vector<String>();
	
	public void añadirElementos() {
		androidsV2.addElement("Cupcake");
		androidsV2.addElement("Donut");
		androidsV2.addElement("Elcair");
		androidsV2.addElement("Froyo");
		androidsV2.addElement("Gingerbread");
		androidsV2.addElement("Honeycomb");
	}
	public void imprimir() {
		System.out.println("Tamaño del Vector " + androidsV2.size());
		System.out.println("ArrayList " + androidsV2);
		System.out.println();
	}
	
	public void eliminar() {
		try {
			androidsV2.remove(3);
			System.out.println("Se a aplicado un remove del elemento con indice 3");
			System.out.println();
			System.out.println("Nuevo tamaño del Vector " + androidsV2.size());
			System.out.println("ArrayList " + androidsV2);
		} catch(ArrayIndexOutOfBoundsException arrayE) {
			System.out.println("Estas intentando eliminar un indice que no existe");
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println();
		}
		
		System.out.println();
	}
}
