package dev.tedesco.opp;

public class Telefono {
	
	//Atributos
	private int id;
	private String marca;
	public String modelo;
	private double precio;
	
	//Constructor
	public Telefono() {
		this.id = 0;
		this.marca = "Motorola";
		this.modelo = "E";
		this.precio = 1;
	}
	
	public Telefono(int id, String marca, double precio) {
		this.id = id;
		this.marca = marca;
		this.precio = precio;
	}
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		if(precio > 1) {
			this.precio = precio;
		} else {
			throw new IllegalArgumentException();
		}
	}

	//Metodos
	public void mostrarDatos() {
		System.out.println("Datos del Telefono");
		System.out.println("ID: " + id);
		System.out.println("Marca: " + marca);
		System.out.println("Modelo: " + modelo);
		System.out.println("Precio: " + precio);
	}
	
}
