package dev.tedesco.opp;

public class Playera extends Ropa implements IDevolucion{
	//Atributos
	private String corte;
	
	//Constructor
	//sobreescribiendo constructor de clase padre
	public Playera(int id, double precio, String talla, String color, String corte) {
		//Siempre se referira a la super clase
		super(id, precio, talla, color);
		//atributo propio
		this.corte = corte;
	}
	
	//Getters y Setters
	public String getCorte() {
		return corte;
	}

	public void setCorte(String corte) {
		this.corte = corte;
	}
	
	//Sobreescribo el metodo mostrarDatos de la superclase
	@Override
	public void mostrarDatos(String nombreClase) {
		super.mostrarDatos(nombreClase);
		System.out.println("Corte: " + corte);
	}
	
	//Sobreescribo el metodo de la interface, obligatorio!
	@Override
	public void hacerDevolucion() {
		// TODO Auto-generated method stub
		System.out.println("Devolucion de una playera");
	}
	
}
