package dev.tedesco.opp;

public class Ropa {
	//Atributos
	private int id;
	private double precio;
	private String talla;
	private String color;
	
	//Constructor
	//defino que para que un objeto sea creado a partir de esta clase, al menos deben definirse estos parametros
	public Ropa(int id, double precio, String talla, String color) {
		this.id = id;
		this.precio = precio;
		this.talla = talla;
		this.color = color;
	}
	//Getter y Setters
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public double getPrecio() {
		return precio;
	}


	public void setPrecio(double precio) {
		this.precio = precio;
	}


	public String getTalla() {
		return talla;
	}


	public void setTalla(String talla) {
		this.talla = talla;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	//Metodos
	public void mostrarDatos(String nombreClase) {
		System.out.println(nombreClase);	
		System.out.println("ID: " + id);
		System.out.println("Precio: " + precio);
		System.out.println("Talla: " + talla);
		System.out.println("Color: " + color);
	}
}
