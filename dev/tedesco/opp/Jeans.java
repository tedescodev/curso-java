package dev.tedesco.opp;

public class Jeans extends Ropa implements IDevolucion{
	//Atributos
	private String corte;
	private char genero;
	
	//Constructor
	public Jeans(int id, double precio, String talla, String color, String corte, char genero) {
		super(id, precio, talla, color);
		this.corte = corte;
		this.genero = genero;
	}
	
	//Getters y Setters
	public String getCorte() {
		return corte;
	}

	public void setCorte(String corte) {
		this.corte = corte;
	}

	public char getGenero() {
		return genero;
	}

	public void setGenero(char genero) {
		this.genero = genero;
	}
	
	//Sobreescribo el metodo mostrarDatos de la superclase
	@Override
	public void mostrarDatos(String nombreClase) {
		super.mostrarDatos(nombreClase);
		System.out.println("Corte: " + corte);
		System.out.println("Genero: " + genero);
	}
	
	//Sobreescribo el metodo de la interface, obligatorio!
	@Override
	public void hacerDevolucion() {
		// TODO Auto-generated method stub
		System.out.println("Devolucion del Jeans");
		
	}
}
