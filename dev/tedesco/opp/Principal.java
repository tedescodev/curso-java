package dev.tedesco.opp;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Telefono miTelefono = new Telefono();
		//miTelefono.mostrarDatos();
		
		Telefono tuTelefono = new Telefono(1, "Samsung", 2.0);
		System.out.println("--------Ejercicio objetos--------");
		System.out.println();
		System.out.println("Precio desde Getter: " + tuTelefono.getPrecio());
		//tuTelefono.mostrarDatos();
		try {
			tuTelefono.setPrecio(-1.0);
			System.out.println("Precio seteado: " + tuTelefono.getPrecio());
		} catch(Exception e){
			System.out.println("El precio no es válido");
		}
		
		Playera playera = new Playera(1, 299.5, "CH", "Gris", "Polo");
		PlayeraPersonalizada playeraPersonalizada = new PlayeraPersonalizada(4, 499.5, "M", "Negro", "@TedescoDev");
		Jeans jeans = new Jeans(2, 799, "32", "Azul", "Skiny", 'F');
		Calcetin calcetin = new Calcetin(3, 29, "M", "Rojo");
		System.out.println();
		
		System.out.println("--------Ejercicio herencia--------");
		System.out.println();
		playera.mostrarDatos("Playera de verano");
		System.out.println();
		playeraPersonalizada.mostrarDatos("Playera @TedescoDev");
		System.out.println();
		jeans.mostrarDatos("Jeans de moda");
		System.out.println();
		calcetin.mostrarDatos("Calcetines");
		System.out.println();
		playera.hacerDevolucion();
		jeans.hacerDevolucion();
	}

}
