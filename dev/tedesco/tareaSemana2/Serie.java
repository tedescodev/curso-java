package dev.tedesco.tareaSemana2;

public class Serie extends Coleccion implements IVisualizable{
	public Serie(String titulo, String genero, String creador, int duracion, int temporadas){
		super(titulo, genero, creador, duracion);
		this.temporadas = temporadas;
	}
	
	private boolean visto = false;
	private int temporadas = 1;
	private int contador = 0; 
	
	public int getTemporadas() {
		return temporadas;
	}
	public void setTemporadas(int temporadas) {
		this.temporadas = temporadas;
	}
	
	public void mostrarDatos(String nombreClase) {

		super.mostrarDatos(nombreClase);
		System.out.println("Visto: " + visto);
		System.out.println("Temporadas: " + temporadas);
	}
	
	public void marcarVisto() {
	
		visto = true;
		System.out.println("Visto cambio de false a true");
		contador ++;
	}
	
	public void esVisto() {
	
		visto = false;
		System.out.println("Visto volvio a su estado por defecto");
		contador ++;
	}
	
	public void tiempoVisto() {
	
		contador = duracion * contador;
		System.out.println("Esta serie se ha visualizado " + contador + " minutos");
	}
}
