package dev.tedesco.tareaSemana2;

import java.util.ArrayList;

public class Ejecutable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Pelicula pelicula1 = new Pelicula("León", "Acción/Drama", "Luc Besson", 110, 1999);
		Pelicula pelicula2 = new Pelicula("Interestelar", "Ciencia Ficción/Aventura", "Christopher Nolan", 209, 2014);
		Pelicula pelicula3 = new Pelicula("La tumba de las luciernagas", "Bélico/Animación", "Isao Takahata", 93, 1988);
		Pelicula pelicula4 = new Pelicula("Escuadrón suicida", "Acción", "David Ayer", 177, 2016);
		Pelicula pelicula5 = new Pelicula("La mujer que cantaba", "Drama/Bélico", "Denis Villeneuve", 130, 2010);
		
		Serie serie1 = new Serie("Desencanto", "Comedia", "Matt Groening", 800, 4);
		Serie serie2 = new Serie("Arrow", "Acción", "Greg Berlanti", 3400, 8);
		Serie serie3 = new Serie("Padre de familia", "Comedia", "Seth MacFarlane", 6980, 18);
		Serie serie4 = new Serie("Oye Arnold", "Comedia", "Craig Bartlett", 2000, 5);
		Serie serie5 = new Serie("Las sombrías aventuras de Billy y Mandy", "Comedia", "Maxwell Atoms", 5200, 6);
		
		//String [] peliculas = new String [5];
		//String [] series = new String [5];
		
		
		//peliculas [0] = "León";
		//peliculas [1] = "Insterestelar";
		//peliculas [2] = "La tumba de las luciernagas";
		//peliculas [3] = "Escuadrón suicida";
		//peliculas [4] = "La mujer que cantaba";
		
		//series [0] = "Desencanto";
		//series [1] = "Arrow";
		//series [2] = "Padre de familia";
		//series [3] = "Oye Arnold";
		//series [4] = "Las sombrías aventuras de Billy y Mandy";
		
		ArrayList<Pelicula> pelicula = new ArrayList<>();
		ArrayList<Serie> serie = new ArrayList<>();
		
		pelicula.add(pelicula1);
		pelicula.add(pelicula2);
		pelicula.add(pelicula3);
		pelicula.add(pelicula4);
		pelicula.add(pelicula5);
		
		serie.add(serie1);
		serie.add(serie2);
		serie.add(serie3);
		serie.add(serie4);
		serie.add(serie5);
		
		
		try{
			pelicula1.marcarVisto();
			pelicula3.marcarVisto();
			pelicula5.marcarVisto();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println();
		}
		
		try{
			serie1.marcarVisto();
			serie2.marcarVisto();
			serie4.marcarVisto();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println();
		}
		
		try{
			System.out.println(pelicula.get(0).getTitulo());
			pelicula1.tiempoVisto();
		}catch(ArrayIndexOutOfBoundsException arrayE) {
			System.out.println("Estas intentando mostrar un indice que no existe");
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println();
		}
		
		try{
			System.out.println(pelicula.get(2).getTitulo());
			pelicula3.tiempoVisto();
		}catch(ArrayIndexOutOfBoundsException arrayE) {
			System.out.println("Estas intentando mostrar un indice que no existe");
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println();
		}
		
		try{
			System.out.println(pelicula.get(4).getTitulo());
			pelicula5.tiempoVisto();
		}catch(ArrayIndexOutOfBoundsException arrayE) {
			System.out.println("Estas intentando mostrar un indice que no existe");
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println();
		}
		
		try{
			System.out.println(serie.get(0).getTitulo());
			serie1.tiempoVisto();
		}catch(ArrayIndexOutOfBoundsException arrayE) {
			System.out.println("Estas intentando mostrar un indice que no existe");
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println();
		}
		
		try{
			System.out.println(serie.get(1).getTitulo());
			serie2.tiempoVisto();
		}catch(ArrayIndexOutOfBoundsException arrayE) {
			System.out.println("Estas intentando mostrar un indice que no existe");
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println();
		}
		
		try{
			System.out.println(serie.get(3).getTitulo());
			serie4.tiempoVisto();
		}catch(ArrayIndexOutOfBoundsException arrayE) {
			System.out.println("Estas intentando mostrar un indice que no existe");
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println();
		}
		
		try{
			System.out.println(pelicula.get(3).getTitulo());
			System.out.println("Esta es la pelicula mas reciente");
		}catch(ArrayIndexOutOfBoundsException arrayE) {
			System.out.println("Estas intentando mostrar un indice que no existe");
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println();
		}
		
		try{
			System.out.println(serie.get(2).getTitulo());
			System.out.println("Esta es la serie con mas temporadas");
		}catch(ArrayIndexOutOfBoundsException arrayE) {
			System.out.println("Estas intentando mostrar un indice que no existe");
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println();
		}
		
		
	}

}
