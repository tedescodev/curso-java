package dev.tedesco.tareaSemana2;

public class Coleccion {
	private String titulo, genero, creador;
	public int duracion;
	
	public Coleccion (String titulo, String genero, String creador, int duracion) {
		
		this.titulo = titulo;
		this.genero = genero;
		this.creador = creador;
		this.duracion = duracion;
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	public String getCreador() {
		return creador;
	}
	public void setCreador(String creador) {
		this.creador = creador;
	}
	
	public int getDuracion() {
		return duracion;
	}
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	
	public void mostrarDatos(String nombreClase){
		System.out.println(nombreClase);
		System.out.println("Titulo: " + titulo);
		System.out.println("Genero: " + genero);
		System.out.println("Creador: " + creador);
		System.out.println("Duración: " + duracion);
	}
}
