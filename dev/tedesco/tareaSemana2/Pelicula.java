package dev.tedesco.tareaSemana2;

public class Pelicula extends Coleccion implements IVisualizable{
		
	public Pelicula(String titulo, String genero, String creador, int duracion, int año){
		super(titulo, genero, creador, duracion);
		this.año = año;
	}

	private boolean visto = false;
	private int año;
	private int contador = 0;
	
	public int getAño() {
		return año;
	}
	public void setAño(int año) {
		this.año = año;
	}
	
	public void mostrarDatos(String nombreClase) {

		super.mostrarDatos(nombreClase);
		System.out.println("Año: " + año);
		System.out.println("Visto: " + visto);
	}
	public void marcarVisto() {
	
		visto = true;
		System.out.println("Visto cambio de false a true");
		contador ++;
	}
	
	public void esVisto() {
	
		visto = false;
		System.out.println("Visto volvio a su estado por defecto");
		contador ++;
	}
	
	public void tiempoVisto() {
	
		contador = duracion * contador;
		System.out.println("Esta pelicula se ha visualizado " + contador + " minutos");
	}
}
