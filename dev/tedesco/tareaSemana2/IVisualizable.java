package dev.tedesco.tareaSemana2;

public interface IVisualizable {
	public void marcarVisto();
	
	public void esVisto();
	
	public void tiempoVisto();
}
