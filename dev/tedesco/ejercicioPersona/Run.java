package dev.tedesco.ejercicioPersona;

public class Run {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Cliente cliente1= new Cliente(1, "Pablo", "Tedesco", 'M', 32, "Montevideo");
		Cliente cliente2= new Cliente(2, "Martin", "Rodriguez", 'M', 32, "Montevideo");
		Empleado empleado= new Empleado(5, "Jorge", "Saravia", 'M', 30, "Ventas");
		
	
		
		System.out.println("--------Tarea semana 2--------");
		System.out.println();
		cliente1.mostrarDatos("Cliente");
		System.out.println();
		cliente2.mostrarDatos("Cliente");
		System.out.println();
		empleado.mostrarDatos("Empleado");
		System.out.println();
		cliente1.hacerReclamacion();
	}

}
