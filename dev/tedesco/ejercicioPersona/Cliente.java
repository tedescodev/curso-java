package dev.tedesco.ejercicioPersona;

public class Cliente extends Persona implements IReclamacion{
	
	private String ciudad;

	public Cliente(int id, String nombre, String apellido, char genero, int edad, String ciudad) {
		super(id, nombre, apellido, genero, edad);
		this.ciudad = ciudad;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	
	@Override
	public void mostrarDatos(String nombreClase) {
		super.mostrarDatos(nombreClase);
		System.out.println("Ciudad: " + ciudad);
	}

	@Override
	public void hacerReclamacion() {
		System.out.println(
			"Reclamacion del cliente " + 
			this.getNombre() + " " + this.getApellido() +
			"con identificacion " + this.getId() +
			" efectuada correctamente"
		);
	}

}
