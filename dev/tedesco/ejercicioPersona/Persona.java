package dev.tedesco.ejercicioPersona;

public class Persona {
	private int id;
	 private String nombre;
	 private String apellido;
	 private char genero;
	 private int edad;
	  
	 public Persona (int id, String nombre, String apellido, char genero, int edad) {
	    this.id = id;
	    this.nombre = nombre;
	    this.apellido = apellido;
	    this.genero = genero;
	    this.edad = edad;
	 }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public char getGenero() {
		return genero;
	}

	public void setGenero(char genero) {
		this.genero = genero;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	public String toString() {
        return nombre;
    }
	 
	//Metodos
	public void mostrarDatos(String nombreClase) {
		System.out.println(nombreClase);	
		System.out.println("ID: " + id);
		System.out.println("Nombre: " + toString());
		System.out.println("Apellido: " + apellido);
		System.out.println("Género: " + genero);
		System.out.println("Edad: " + edad);
	}
}
