package dev.tedesco.ejercicioPersona;

public class Empleado extends Persona{
	private String sector;
	public Empleado(int id, String nombre, String apellido, char genero, int edad, String sector) {
		super(id, nombre, apellido, genero, edad);
		this.sector = sector;
	}
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	
	@Override
	public void mostrarDatos(String nombreClase) {
		super.mostrarDatos(nombreClase);
		System.out.println("Sector: " + sector);
	}
}
