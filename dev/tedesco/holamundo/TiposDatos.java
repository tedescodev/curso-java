package dev.tedesco.holamundo;

public class TiposDatos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//TIPOS DE DATOS
		
		//primitivos
		short año = 0;
		byte edad = 32;
		int id = 54;
		long hora = 2536548;
		
		//de punto flotante
		float estatura = 1.80F;
		double resultado = 932.654656;
		
		//letras o 	cadenas
		char genero = 'M';
		String nombre = "Pablo";
		
		//logico
		boolean status = false;
		boolean existe = true;
		
		//arreglos
		
		//de 1 dimension
		int[] miArregloInt = new int [5];
		miArregloInt[0] = 5;
		miArregloInt[1] = 8;
		miArregloInt[2] = 10;
		miArregloInt[3] = 14;
		miArregloInt[4] = 18;
		System.out.println("Desde mi arreglo int de 1 dimension: " + miArregloInt[2]);
		
		//de 2 dimensiones
		double[][] miArregloDouble2D = new double[3][3]; //matriz i j
		miArregloDouble2D[0][0] = 4.56;
		miArregloDouble2D[0][1] = 5.60;
		miArregloDouble2D[0][2] = 8.30;
		miArregloDouble2D[1][0] = 9.12;
		miArregloDouble2D[1][1] = 3.45;
		miArregloDouble2D[1][2] = 2.60;
		miArregloDouble2D[2][0] = 7.40;
		miArregloDouble2D[2][1] = 8.11;
		miArregloDouble2D[2][2] = 2.20;
		System.out.println("Desde mi arreglo double de 2 dimensiones: " + miArregloDouble2D[1][2]);
		
		//de 3 dimensiones
		char[][][] miArregloChar3D = new char[2][2][2];
		miArregloChar3D[0][0][0] = 'a';
		miArregloChar3D[0][0][1] = 'b';
		miArregloChar3D[0][1][0] = 'c';
		miArregloChar3D[0][1][1] = 'd';
		miArregloChar3D[1][0][0] = 'e';
		miArregloChar3D[1][0][1] = 'f';
		miArregloChar3D[1][1][0] = 'g';
		miArregloChar3D[1][1][1] = 'h';
		System.out.println("Desde mi arreglo char de 3 dimensiones: " + miArregloChar3D[1][1][0]);

	}

}
