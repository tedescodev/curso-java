package dev.tedesco.holamundo;

public class TareaSemana1 {

	public static void main(String[] args) {
		metodo1();
		metodo2();
	}
	static void metodo1() {
		int matriz[][] = new int[4][4];	
		int par=0;
	    for (int i = 0; i < 4; i++) {
		    for (int j = 0; j < 4; j++) {
		      matriz[i][j] = par += 2;			//Llenar Matriz
		    }
		}
		
		for (int a=0; a < 4; a++) {
			for (int b=0; b < matriz[a].length; b++) {
				System.out.print (matriz[a][b]); 
				if (b!=3) System.out.print("\t"); 
			}											//Visualizar Matriz
			System.out.println("\t");
		}
	}
	static void metodo2() {
		int num = 2, cont = 1, topeF = 4, topeC = 4;

		int[][] numeroCuadrado = new int[topeF][topeC];

		// Llenar matriz

		for (int j = 0; j < topeC; j++) {
			System.out.println();
			System.out.print(cont + "|   ");
			cont++;
			for (int i = 0; i < topeF; i++) {
				numeroCuadrado[i][j] = num;
				System.out.print(numeroCuadrado[i][j] + "   ");
				num = num + 2;
			}
		}
	}
}
