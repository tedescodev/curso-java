package dev.tedesco.holamundo;

public class Operadores {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a = 2;
		int b = 5;
		double c = 0;
		
		//Suma
			c = a + b;
			System.out.println("Suma de a + b = " + c);
		
		//Resta
			c = a - b;
			System.out.println("Resta de a - b = " + c);
		
		//Multiplicacion
			c = a * b;
			System.out.println("Multiplicacion de a x b = " + c);
		
		//Division
			c = a / b;
			System.out.println("Division de a / b = " + c);
		
		//Modulo
			c = a % b;
			System.out.println("El resto de a dividido b = " + c);
		
		//Asignacion con operacion
			c = 2;
			
			c += a;
			System.out.println("c += a = " + c);
			
			c -= a;
			System.out.println("c -= a = " + c);
			
			c *= a;
			System.out.println("c *= a = " + c);
			
			c /= a;
			System.out.println("c /= a = " + c);
			
			c %= a;
			System.out.println("c %= a = " + c);
		
		//Operador incremento
			int i = 3;
			
			//prefijo
			++i; //i + 1
			//1. Incrementando
			//2. Asignando
			System.out.println("Prefijo: i = " + i);
			
			//posfijo
			i++; //i = i + 1
			// 1. Asignando
			// 2. Incrementando
			System.out.println("Posfijo: i = " + i);
			
			System.out.println("Impresion prefijo: i = " + (++i));
			System.out.println("Impresion posfijo: i = " + (i++));
			System.out.println("i es = " + i);
		
		//Equidad
			a = 2;
			b = 5;
			System.out.println(a == b);
			System.out.println(a != b);
		
		//Relacionales
			System.out.println(a < b);
			System.out.println(a > b);
			System.out.println(a >= b);
			a = 5;
			System.out.println(a >= b);
		
		//Logicos
			boolean f = false;
			boolean g = true;
			//AND //Se tienen que cumplir ambas
			System.out.println("Logicos: " + (f && g));
			
			//OR //Si se cumple al menos una
			System.out.println("Logicos: " + (f || g));
			
			//NOT //Niega
			System.out.println("Logicos: " + (!f));
			System.out.println("Logicos: " + (!g));
	}

}
