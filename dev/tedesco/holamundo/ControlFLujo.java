package dev.tedesco.holamundo;

public class ControlFLujo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a = 4, b = 6;
		
		//IF ELSE
			if(a > b) {
				System.out.println("a es mayor que b");
			} else if(b > a) {
				System.out.println("b es mayor que a");
			} else {
				System.out.println("ambos son iguales");
			}
		
		//SWITCH
			switch(a) {
			case 1:
				System.out.println("si, a es 1");
				break;
			case 2:
				System.out.println("si, a es 2");
				break;
			case 3:
				System.out.println("si, a es 3");
				break;
			case 4:
				System.out.println("si, a es 4");
				break;
			default:
				System.out.println("a no es ninguno de esos valores");
				break;
			}
		
		//WHILE
			a = 1;
			while(a < 5) {
				System.out.println("WHILE: si, a = " + a + " es menor a 5");
				a++;
			}
		
		//FOR
			//1
			for(int i = 1; i <= 5; i++) {
				switch(i) {
				case 1:
					System.out.println("FOR: si, i = " + i + " por lo tanto es menor a 5");
					break;
				case 2:
					System.out.println("FOR: si, i = " + i + " por lo tanto es menor a 5");
					break;
				case 3:
					System.out.println("FOR: si, i = " + i + " por lo tanto es menor a 5");
					break;
				case 4:
					System.out.println("FOR: si, i = " + i + " por lo tanto es menor a 5");
					break;
				case 5:
					System.out.println("FOR: si, i = " + i);
					break;
				default:
					System.out.println("lo siento, i es mayor a 5");
					break;
				}
				
			}
			
			//2
			int[] miArreglo =  new int[5];
			for(int z = 1; z < miArreglo.length; z++){
				miArreglo[z] = z * 2;
				System.out.println("FOR ARREGLO: " + miArreglo[z]);
			}
			
			//3 foreach
			for(int k : miArreglo) {
				System.out.println("FOR EACH: " + k);
			}
			
		
	}

}
